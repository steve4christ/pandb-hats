package com.celestial.pandbhats.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.sql.CallableStatement;

public class UserDAO
{
    Connection con = null;

    private static String linkTableName = "link";
    private static String joiningTable = "linkToMetatag";
    private static String tagTableName = "metatag";

    public void loginUser(String name, String pwd) throws SQLException
    {
        PreparedStatement saveStmt = null;
        String linkSql = "insert into " + linkTableName + " (link_name, link_description) values( ?, ?)";
        try
        {
            String[] tags = {};
            con = DBConnector.getConnector().getConnection();
            con.setAutoCommit(false);
            saveStmt = con.prepareStatement(linkSql);
            saveStmt.setString(1, name);
            saveStmt.setString(2, pwd);
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            CallableStatement cStmt = con.prepareCall("{call saveTag(?)}");
            int linkId = getCurrentIDNum() + 1;
            for (String tag : tags)
            {
                String query = "insert into linkToMetatag (tag_name, link_id) values ('" + tag + "', " + linkId + " )";
                stmt.addBatch(query);
                cStmt.setString(1, tag);
                cStmt.addBatch();   
            }
            saveStmt.executeUpdate();
            stmt.executeBatch();
            cStmt.executeBatch();
            con.commit();
            stmt.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private int getCurrentIDNum()
    {
        int result = 0;
        try
        {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select link_id from " + linkTableName);
            while (rs.next())
            {
                int idNum = rs.getInt("link_id");
                if (idNum > result)
                {
                    result = idNum;
                }
            }
        } catch (SQLException se)
        {
            System.out.println(se);
        }
        return result;
    }
}
